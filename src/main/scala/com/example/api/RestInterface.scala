package com.example.api

import akka.actor._

import spray.routing._
import spray.http.StatusCodes
import spray.httpx.SprayJsonSupport._
import spray.routing.RequestContext
import akka.util.Timeout
import scala.concurrent.duration._
import com.yammer.metrics.Metrics
import com.yammer.metrics.scala.Timer

class RestInterface extends Actor
//                    with HttpServiceActor
with RestApi {
  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  def actorRefFactory = context

  // this actor only runs our route, but you could add
  // other things here, like request stream processing,
  // timeout handling or alternative handler registration
  def receive = runRoute(routes)
}

trait RestApi extends HttpService {
  // we use the enclosing ActorContext's or ActorSystem's dispatcher for our Futures and Scheduler
  implicit def executionContext = actorRefFactory.dispatcher

//  import com.example.api.TicketProtocol._
  import com.example.api.BookingProtocol._

  implicit val timeout = Timeout(10 seconds)

  val metricInitAvail = Metrics.defaultRegistry().newTimer(classOf[HotelManager], "initAll")
  val timerInitAvail = new Timer(metricInitAvail)

  import akka.pattern.ask
  import akka.pattern.pipe

  val ticketMaster = actorRefFactory.actorOf(Props[TicketMaster])
  val hotelManager = actorRefFactory.actorOf(Props(classOf[HotelManager],0L))
  val customerManager = actorRefFactory.actorOf(Props[CustomerManager])

  def routes: Route =
    pathPrefix("hotel" / IntNumber) {
      hotel =>
        pathPrefix("room" / Segment) {
          roomId =>
            path("avail") {
              get {
                requestContext =>
                  val responder = createResponder(requestContext, hotelManager)
                  hotelManager.ask(GetRoomAvails(hotel, roomId)).pipeTo(responder)
              }
            }
        }
    } ~
//      path("events") {
//        put {
//          entity(as[Event]) {
//            event => requestContext =>
//              val responder = createResponder(requestContext, ticketMaster)
//              ticketMaster.ask(event).pipeTo(responder)
//          }
//        } ~
//          get {
//            requestContext =>
//              val responder = createResponder(requestContext, ticketMaster)
//              ticketMaster.ask(GetEvents).pipeTo(responder)
//          }
//      } ~
//      path("ticket") {
//        get {
//          entity(as[TicketRequest]) {
//            ticketRequest => requestContext =>
//              val responder = createResponder(requestContext, ticketMaster)
//              ticketMaster.ask(ticketRequest).pipeTo(responder)
//          }
//        }
//      } ~
//      path("ticket" / Segment) {
//        eventName => requestContext =>
//          val req = TicketRequest(eventName)
//          val responder = createResponder(requestContext, ticketMaster)
//          ticketMaster.ask(req).pipeTo(responder)
//      } ~
      path("avails" / IntNumber) {
        hotel =>
        put {
          entity(as[Avail]) {
            event => requestContext =>
              val responder = createResponder(requestContext, hotelManager)
              hotelManager.ask(event).pipeTo(responder)
          }
        } ~
          get {
            requestContext =>
              val responder = createResponder(requestContext, hotelManager)
              hotelManager.ask(GetAvails(hotel)).pipeTo(responder)
          }
      } ~
      path("booking") {
        get {
          entity(as[BookingRequest]) {
            ticketRequest => requestContext =>
              val responder = createResponder(requestContext, hotelManager)
              hotelManager.ask(ticketRequest).pipeTo(responder)
          }
        }
      } ~
      pathPrefix("hotel" / IntNumber) {
        hotel =>
          pathPrefix("room" / Segment) {
            room =>
              path("date" / Segment) {
                date => requestContext =>
                  val req = BookingRequest(hotel, room, date)
                  val responder = createResponder(requestContext, hotelManager)
                  hotelManager.ask(req).pipeTo(responder)
              }
          }
      } ~
      path("hotels") {
        get {
          requestContext =>
            val responder = createResponder(requestContext, hotelManager)
            hotelManager.ask(GetHotels).pipeTo(responder)
        }
      } ~
      path("init" / IntNumber) {
        custId =>
          requestContext =>
            val responder = createResponder(requestContext, customerManager)
            val req = InitHotels(custId)
            customerManager.ask(req).pipeTo(responder)
      }~
      path("random" / IntNumber) {
        nbHotel =>
          requestContext =>
            val responder = createResponder(requestContext, customerManager)
            val req = InitRandomHotels(nbHotel)
            customerManager.ask(req).pipeTo(responder)
      }

//  def matchCustId(custId:Long):ActorRef = {
//    actorRefFactory.child(room) match {
//      case Some(roomManager) => {
//        log.debug("send avails to roomManager")
//        roomManager ! Avail(hotel, room, date, qty)
//      }
//      case None => {
//        log.debug(s"create Room Manage for room ${room}")
//        val roomManager = createRoom(room)
//        log.debug("send avails to roomManager")
//        roomManager ! Avail(hotel, room, date, qty)
//      }
//    }

  def createResponder(requestContext: RequestContext, actorRef: ActorRef) = {
    actorRefFactory.actorOf(Props(classOf[Responder],requestContext, actorRef))
  }

}

class Responder(requestContext: RequestContext, ticketMaster: ActorRef) extends Actor with ActorLogging {

  import TicketProtocol._
  import BookingProtocol._
  import spray.httpx.SprayJsonSupport._

  def receive = {

    case ticket: Ticket =>
      requestContext.complete(StatusCodes.OK, ticket)
      self ! PoisonPill

    case EventCreated =>
      requestContext.complete(StatusCodes.OK)
      self ! PoisonPill

    case TicketProtocol.SoldOut =>
      requestContext.complete(StatusCodes.NotFound)
      self ! PoisonPill

    case Events(events) =>
      requestContext.complete(StatusCodes.OK, events)
      self ! PoisonPill

    case HotelList(hotels) =>
      requestContext.complete(StatusCodes.OK, hotels)
      self ! PoisonPill


    case ticket: Booking =>
      requestContext.complete(StatusCodes.OK, ticket)
      self ! PoisonPill

    case AvailCreated =>
      requestContext.complete(StatusCodes.OK)
      self ! PoisonPill

    case InitDone =>
      requestContext.complete(StatusCodes.OK)
      self ! PoisonPill

    case BookingProtocol.SoldOut =>
      requestContext.complete(StatusCodes.NotFound)
      self ! PoisonPill

    case Avails(avails) =>
      requestContext.complete(StatusCodes.OK, avails)
      self ! PoisonPill

  }
}