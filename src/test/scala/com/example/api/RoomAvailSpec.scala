package com.example.api

import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import akka.testkit.{ImplicitSender, TestKit}
import akka.actor.{Props, ActorSystem}

class RoomAvailSpec extends TestKit(ActorSystem("testTickets"))
with WordSpec
with MustMatchers
with ImplicitSender
with StopSystemAfterAll {
  "The RoomAvail" must {
    "Give out booking until they are sold out" in {
      import BookingProtocol._
      val hotel = 1
      val date = "20130101"
      def mkAvails(room:String, date:String) = (1 to 10).map(i=>Avail(hotel, room, date, i)).toList
      val roomAvail = system.actorOf(RoomAvail(date))

      roomAvail ! Avails(mkAvails("room1", date))
      roomAvail ! BookRoom

      expectMsg(Avail(hotel, "room1", date, 1))

      val nrs = (2 to 10)
      nrs.foreach(_ => roomAvail ! BookRoom)

      val tickets = receiveN(9)
      tickets.zip(nrs).foreach { case (ticket:Avail, nr) => ticket.qty must be(nr) }

      roomAvail ! BookRoom
      expectMsg(SoldOut)
    }

    "Give avails" in {
      import BookingProtocol._
      val hotel = 1
      val date = "20130101"

      def mkAvails(room:String, date:String) = (1 to 10).map(i=>Avail(hotel, room, date, i)).toList
      val roomAvail = system.actorOf(RoomAvail(date))

      roomAvail ! Avails(mkAvails("room1", date))
      roomAvail ! GetAvails(hotel)

      expectMsg(10)

    }
  }
}
