package com.example

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import spray.can.Http
import com.typesafe.config.ConfigFactory
import com.example.api.RestInterface


object Boot extends App  {

  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

//  this: ServerCore with Api =>

  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("on-spray-can")
  // create and start our service actor
  val api = system.actorOf(Props[RestInterface], "httpInterface")
//  val service = system.actorOf(Props[MyServiceActor], "my-service")

  // create a new HttpServer using our handler tell it where to bind to
//  newHttpServer(service) ! Bind(interface = "localhost", port = 8080)
  IO(Http) ! Http.Bind(api, interface = host, port = port)
}