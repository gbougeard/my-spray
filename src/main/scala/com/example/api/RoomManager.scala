package com.example.api

import akka.actor._
import scala.Some
import concurrent.Future
import scala.concurrent.duration._
import akka.util.Timeout
import akka.event.LoggingReceive
import com.example.api.TicketProtocol.{Events, Event, GetEvents}
import com.example.metrics.MyMetrics

object RoomManager{
  def apply(name:String): Props =
  Props(classOf[RoomManager],name)
}

class RoomManager(name:String) extends Actor with CreateRoomAvail with ActorLogging {

  import BookingProtocol._
  import context._

  implicit val timeout = Timeout(5 seconds)

  override def preStart() = MyMetrics.countRoomManager += 1

  val room = name

  def receive = LoggingReceive{

    case Avail(hotel, r, date, qty) =>
      log.debug(s"Adding avail for room $r on date $date qty: $qty.")
      context.child(date) match {
        case Some(roomAvail) => {
          log.debug("send avails to roomAvail")
          val avails = Avails((1 to qty).map(nr => Avail(hotel, room, date, nr)).toList)
          roomAvail ! avails
        }
        case None => {
          log.debug(s"create Room Avail for date $date")
          val roomAvail = createRoomAvail(date)

          val avails = Avails((1 to qty).map(nr => Avail(hotel, room, date, nr)).toList)
          roomAvail ! avails
        }
      }

      sender ! AvailCreated

    case BookingRequest(hotel, r, date) =>
      log.debug(s"Getting a room $r for the date $date .")

      context.child(date) match {
        case Some(roomAvail) => roomAvail.forward(BookRoom)
        case None => sender ! SoldOut
      }

    case GetAvails(hotel) =>
      import akka.pattern.ask
      val capturedSender = sender
      log.debug(s"GetAvails $sender")

      def askAndMapToEvent(hotel:Long, room:String, ticketSeller:ActorRef) =  {

        val futureInt = ticketSeller.ask(GetAvails(hotel)).mapTo[Int]

        futureInt.map(nrOfTickets => Avail(hotel, room, ticketSeller.actorRef.path.name, nrOfTickets))
      }
      val futures = context.children.map(ticketSeller => askAndMapToEvent(hotel, room, ticketSeller))

      Future.sequence(futures).map { avails =>
        log.debug(s"avails ${avails.toList}")
        capturedSender ! Avails(avails.toList)
      }

  }

}

trait CreateRoomAvail{ self:Actor =>
  def createRoomAvail(name:String) =  context.actorOf(RoomAvail(name), name)
}
