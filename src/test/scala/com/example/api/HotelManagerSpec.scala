package com.example.api

import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import akka.testkit.{ImplicitSender, TestKit}
import akka.actor.{Props, ActorSystem}

class HotelManagerSpec extends TestKit(ActorSystem("testTickets"))
with WordSpec
with MustMatchers
with ImplicitSender
with StopSystemAfterAll {
  "The HotelManager" must {
    "Give out booking until they are sold out" in {
      import BookingProtocol._

      //      def mkAvails(room: String, date: String) = (1 to 10).map(i => Avail(room, date, i)).toList

      val hotel = 1
      val room = "room1"
      val date = "20130101"

      val hotelManager = system.actorOf(Props(new HotelManager(hotel)))

      hotelManager ! Avail(hotel, room, date, 10)
      expectMsg(AvailCreated)
    }

    "Give avails" in {
      import BookingProtocol._

      //      def mkAvails(room: String, date: String) = (1 to 10).map(i => Avail(hotel, room, date, i)).toList
      val hotel = 1
      val room1 = "room1"
      val room2 = "room2"
      val date1 = "20130101"
      val date2 = "20130601"

      val hotelManager = system.actorOf(Props(new HotelManager(hotel)))

      hotelManager ! Avail(hotel, room1, date1, 10)
      expectMsg(AvailCreated)

      hotelManager ! Avail(hotel, room1, date2, 5)
      expectMsg(AvailCreated)

      hotelManager ! Avail(hotel, room2, date2, 1)
      expectMsg(AvailCreated)

      hotelManager ! GetAvails(hotel)
      expectMsg(Avails(List(Avail(hotel, room1, date1, 10),
        Avail(hotel, room1, date2, 5),
        Avail(hotel, room2, date2, 1))))

      //      expectMsg(10)

    }
    "Give avails for a room" in {
      import BookingProtocol._

      //      def mkAvails(room: String, date: String) = (1 to 10).map(i => Avail(room, date, i)).toList
      val hotel = 1
      val room = "room1"
      val date1 = "20130101"
      val date2 = "20130601"

      val hotelManager = system.actorOf(Props(new HotelManager(hotel)))

      hotelManager ! Avail(hotel, room, date1, 10)
      expectMsg(AvailCreated)

      hotelManager ! Avail(hotel, room, date2, 5)
      expectMsg(AvailCreated)

      hotelManager ! GetRoomAvails(hotel, room)
      expectMsg(Avails(List(Avail(hotel, room, date1, 10), Avail(hotel, room, date2, 5))))
      //      expectMsg(10)

    }
  }
}
