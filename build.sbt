import AssemblyKeys._
import com.typesafe.startscript.StartScriptPlugin

organization  := "com.example"

version       := "0.1-SNAPSHOT"

scalaVersion  := "2.10.2"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

resolvers ++= Seq("Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
                  "Sonatype snapshots"  at "http://oss.sonatype.org/content/repositories/snapshots/",
                  "Spray Repository"    at "http://repo.spray.io",
                  "Spray Nightlies"     at "http://nightlies.spray.io/")

libraryDependencies ++= {
    val akkaVersion       = "2.2.1"
    val sprayVersion      = "1.2-20130822"
  Seq(
      "io.spray"            %   "spray-can"     % sprayVersion,
      "io.spray"            %   "spray-routing" % sprayVersion,
      "io.spray"            %   "spray-testkit" % sprayVersion,
      "io.spray"            %% "spray-json"      % "1.2.5",
      "com.typesafe.akka"   %%  "akka-actor"    % akkaVersion,
      "com.typesafe.akka"   %% "akka-slf4j"      % akkaVersion,
      "com.typesafe.akka"   %%  "akka-testkit"   % akkaVersion   % "test",
      "com.typesafe.akka"   %%  "akka-multi-node-testkit" % akkaVersion   % "test",
      "org.specs2"          %%  "specs2"        % "1.13" % "test",
      "ch.qos.logback"      %  "logback-classic" % "1.0.10",
      "org.scalatest"       %% "scalatest"       % "1.9.1"       % "test",
      "mysql"               % "mysql-connector-java" % "5.1.24",
      "com.github.tototoshi" %% "slick-joda-mapper" % "0.3.0",
      "com.typesafe.slick" %% "slick" % "1.0.1" ,
      "org.slf4j" % "slf4j-api" % "1.7.2" ,
      "ch.qos.logback" % "logback-classic" % "1.0.9"  ,
      // metrics
      "nl.grons" % "metrics-scala_2.10" % "2.2.0"
    )
  }

seq(Revolver.settings: _*)

// Assembly settings
mainClass in Global := Some("com.example.Boot")

jarName in assembly := "bookings-server.jar"

assemblySettings

atmosSettings

// StartScript settings
seq(StartScriptPlugin.startScriptForClassesSettings: _*)