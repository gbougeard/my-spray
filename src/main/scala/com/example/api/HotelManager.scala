package com.example.api

import akka.actor._
import scala.Some
import scala.concurrent.Future
import scala.concurrent.duration._
import akka.util.Timeout
import akka.event.LoggingReceive
import com.example.domain
import com.example.domain.HotelCatInvs
import com.example.metrics.MyMetrics

object HotelManager{
  def apply(hotelId:Long): Props =
    Props(classOf[HotelManager],hotelId)
}

class HotelManager(hotelId:Long) extends Actor with CreateRoom with ActorLogging {

  import BookingProtocol._
  import context._

  implicit val timeout = Timeout(5 seconds)

  val name = hotelId

  import com.yammer.metrics.Metrics
  import com.yammer.metrics.scala.Timer

  val metricInitAvail = Metrics.defaultRegistry().newTimer(classOf[HotelManager], "initAvail")
  val timerInitAvail = new Timer(metricInitAvail)
  val metricSetAvail = Metrics.defaultRegistry().newTimer(classOf[HotelManager], "setAvail")
  val timerSetAvail = new Timer(metricSetAvail)
  val metricGetAvails = Metrics.defaultRegistry().newTimer(classOf[HotelManager], "getAvails")
  val timerGetAvails = new Timer(metricGetAvails)
  val metricGetRoomAvails = Metrics.defaultRegistry().newTimer(classOf[HotelManager], "getRoomAvails")
  val timerGetRoomAvails = new Timer(metricGetRoomAvails)
  val metricBookingRequest = Metrics.defaultRegistry().newTimer(classOf[HotelManager], "BookingRequest")
  val timerBookingRequest = new Timer(metricBookingRequest)

  override def preStart() = MyMetrics.countHotelManager += 1


  def receive = LoggingReceive {

    case Avail(hotel, room, date, qty) =>
      log.debug(s"Adding avail for room $room on date $date qty: $qty.")
      timerSetAvail.time(setAvail(hotel, room, date, qty))

    case BookingRequest(hotel, room, date) =>
      log.info(s"Getting a room $room for the date $date .")

      context.child(room) match {
        case Some(roomAvail) => roomAvail.forward(BookRoom)
        case None => sender ! SoldOut
      }

    case GetRoomAvails(hotel, room) =>
      timerGetRoomAvails.time(getRoomAvail(hotel, room))

    case GetAvails(hotel) =>
     timerGetAvails.time(getAvails(hotel))

    case GetHotels =>
      val hotels = HotelList(domain.Hotels.findAll.map {
        h =>
          BookingProtocol.Hotels(h.id, h.name, h.city)
      })
      sender ! hotels

    case InitAvail(id) =>
      log.info(s"Init avail for $id .")
      timerInitAvail.time(initAvail(id))

  }

  private def getAvails(hotel:Long) = {
    import akka.pattern.ask
    val capturedSender = sender

    def askAndMapToEvent(hotel:Long, roomAvail: ActorRef) = {
      log.debug(s"askAndMapToEvent ${roomAvail.actorRef.path.name}")
      roomAvail.ask(GetAvails(hotel)).mapTo[Avails]
      //        log.debug(s"self ${self.path.name} child ${roomAvail.actorRef.path.name}")
      //        futureInt.map(avails => Avail(self.path.name, roomAvail.actorRef.path.name, avails))
      //future.map(avails => avails) //Avail(room, ticketSeller.actorRef.path.name, nrOfTickets))
    }

    val futures = context.children.map(roomAvail => askAndMapToEvent(hotel, roomAvail))

    Future.sequence(futures).map {
      events =>
        val avails = events.map {
          avails => avails.avails
        }
        capturedSender ! Avails(avails.flatten.toList)
    }
  }

  private def getRoomAvail(hotel:Long, room: String) = {
    import akka.pattern.ask
    import akka.pattern.pipe

    val capturedSender = sender
    log.debug(s"GetRoomAvails $room")

    context.child(room) match {
      case Some(roomManager) => {
        val future: Future[Avails] = ask(roomManager, GetAvails(hotel)).mapTo[Avails]
        future pipeTo capturedSender
        future.map {
          x => log.debug(s"$x")
        }
      }
      case None => {
        capturedSender ! SoldOut
      }
    }
  }

  private def initAvail(id: Long) = {
    log.debug(s"Init avail for $id .")
    HotelCatInvs.findByHotelId(id).map {
      avail =>
        setAvail(id, avail.catCode.toString, avail.from.toString("YYYYMMdd"), avail.avail)
    }
    log.info(s"InitDone for Hotel $id")
    sender ! InitDone
  }

  private def setAvail(hotel:Long, room: String, date: String, qty: Int) = {
    log.debug(s"setAvail $room $date $qty")
    context.child(room) match {
      case Some(roomManager) => {
        log.debug("send avails to roomManager")
        roomManager ! Avail(hotel, room, date, qty)
      }
      case None => {
        log.debug(s"create Room Manage for room $room")
        val roomManager = createRoom(room)
        log.debug("send avails to roomManager")
        roomManager ! Avail(hotel, room, date, qty)
      }
    }

    sender ! AvailCreated
  }
}

trait CreateRoom {
  self: Actor =>
  def createRoom(name: String) = context.actorOf(RoomManager(name), name)
}
