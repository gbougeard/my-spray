package com.example.api

import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec
import akka.testkit.{ImplicitSender, TestKit}
import akka.actor.{Props, ActorSystem}

class RoomManagerSpec extends TestKit(ActorSystem("testTickets"))
with WordSpec
with MustMatchers
with ImplicitSender
with StopSystemAfterAll {
  "The RoomManager" must {
    "Give out booking until they are sold out" in {
      import BookingProtocol._

      //      def mkAvails(room: String, date: String) = (1 to 10).map(i => Avail(room, date, i)).toList
      val hotel = 1
      val room = "room1"
      val date = "20130101"
      val roomManager = system.actorOf(Props(new RoomManager(room)))

      roomManager ! Avail(hotel, room, date, 10)
      expectMsg(AvailCreated)

      roomManager ! BookingRequest(hotel, room, date)
      expectMsg(Avail(hotel, room, date, 1))

      val nrs = (2 to 10)
      nrs.foreach(_ => roomManager ! BookingRequest(hotel, room, date))

      val tickets = receiveN(9)
      tickets.zip(nrs).foreach {
        case (ticket: Avail, nr) => ticket.qty must be(nr)
      }

      roomManager ! BookingRequest(hotel, room, date)
      expectMsg(SoldOut)
    }

    "Give avails" in {
      import BookingProtocol._

      //      def mkAvails(room: String, date: String) = (1 to 10).map(i => Avail(hotel, room, date, i)).toList
      val hotel = 1
      val room = "room1"
      val date1 = "20130101"
      val date2 = "20130601"
      val roomManager = system.actorOf(Props(new RoomManager(room)))

      roomManager ! Avail(hotel, room, date1, 10)
      expectMsg(AvailCreated)

      roomManager ! Avail(hotel, room, date2, 5)
      expectMsg(AvailCreated)


      roomManager ! GetAvails(hotel)
      expectMsg(Avails(List(Avail(hotel, room, date1, 10), Avail(hotel, room, date2, 5))))
      //      expectMsg(10)

    }
  }
}
