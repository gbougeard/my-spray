package com.example.api

/**
 * Created by gbougeard on 12/06/13.
 */
object BookingProtocol {

  import spray.json._

  case class Avail(hotelId:Long, room: String, date: String, qty: Int)

  case class GetAvails(hotelId:Long)

  case class GetRoomAvails(hotelId:Long, room: String)

  case class Avails(avails: List[Avail])

  case object AvailCreated

  case class BookingRequest(hotelId:Long, room: String, date: String)

  case object SoldOut

  case class Bookings(bookings: List[Bookings])

  case object BookRoom

  case class Booking(hotelId:Long, room: String, date: String, qty: Int)

  case object GetHotels

  case class Hotels(id: Long, name: String, city: String)

  case class HotelList(events: List[Hotels])

  case class InitAvail(hotelId: Long)

  case object InitDone

  case class InitHotels(custId: Long)
  case class InitRandomHotels(nbHotel: Long)

  //----------------------------------------------
  // JSON
  //----------------------------------------------

  object Avail extends DefaultJsonProtocol {
    implicit val format = jsonFormat4(Avail.apply)
  }

  object BookingRequest extends DefaultJsonProtocol {
    implicit val format = jsonFormat3(BookingRequest.apply)
  }

  object Booking extends DefaultJsonProtocol {
    implicit val format = jsonFormat4(Booking.apply)
  }

  object Hotels extends DefaultJsonProtocol {
    implicit val format = jsonFormat3(Hotels.apply)
  }


}
